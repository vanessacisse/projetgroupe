#include"Graphe.h"
#include<iostream>
using namespace std;

int main(){
   
   Graph G;
   cout<<endl;
   int continuer=1;
   do{

     G.initialiser();
     G.dijkstra();
     cout<<endl;
     cout<<"*****Resultats******"<<endl;
     G.afficher();
     cout<<"Continuer(1/0)?"<<endl;
     cin>>continuer;

   }
   while(continuer==1);
 return 0;
}
