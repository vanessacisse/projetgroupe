#ifndef GRAPHE_H
#define GRAPHE_H
#include<iostream>
using namespace std;

class Graph
{
  private:
   int** g;
   int n;
   int * pred;
   int* distmin;
   bool* distrouve;
   int source;
   int destination;

  public:
  Graph();
  void initialiser();
  void dijkstra();
  void afficher();
  void imprimerchemin(int noeud);
};
#endif//GRAPHE_H
